#!/usr/bin/env ruby

require_relative "lib/view"
require_relative "lib/view_counter"
require_relative "lib/view_counter_reporter"

view_counter = ViewCounter.new

ARGV.each do |arg|
  File.foreach(arg) { |line| view_counter.add(View.parse(line)) }
end

reporter = ViewCounterReporter.new
puts "Visit Count"
puts "==========="
reporter.report_visit_count(view_counter, $stdout)
puts ""
puts "Unique Visits"
puts "============="
reporter.report_unique_visits(view_counter, $stdout)
