require "spec_helper"

require "view"
require "view_counter"
require "view_counter_reporter"

RSpec.describe "ViewCounterReporter" do
  describe "report_visit_count" do
    it "it returns no output for view counter with nothing counted" do
      vc = ViewCounter.new
      vcr = ViewCounterReporter.new
      output = StringIO.new("", "w")
      vcr.report_visit_count(vc, output)
      expect(output.string).to eq ""
    end
    it "formats counted_views for a ViewCounter in sorted order, descending by page view count" do
      vc = ViewCounter.new
      vc.add(View.new("p1", "v1"))
      vc.add(View.new("p1", "v2"))
      vc.add(View.new("p2", "v2"))

      vcr = ViewCounterReporter.new
      output = StringIO.new("", "w")
      vcr.report_visit_count(vc, output)

      expect(output.string).to eq "p1 2 visits\np2 1 visit\n"
    end
  end
  describe "report_unique_visits" do
    it "it returns no output for a view counter with nothing counted" do
      vc = ViewCounter.new
      vcr = ViewCounterReporter.new
      output = StringIO.new("", "w")
      vcr.report_unique_visits(vc, output)
      expect(output.string).to eq ""
    end

    it "it reports number of unique views per page" do
      vc = ViewCounter.new
      vc.add(View.new("p1", "v1"))
      vc.add(View.new("p1", "v1"))
      vc.add(View.new("p1", "v1"))

      vc.add(View.new("p1", "v2"))

      vc.add(View.new("p1", "v3"))

      vc.add(View.new("p2", "v1"))
      vc.add(View.new("p2", "v1"))
      vc.add(View.new("p2", "v2"))

      vcr = ViewCounterReporter.new
      output = StringIO.new("", "w")
      vcr.report_unique_visits(vc, output)
      expect(output.string).to eq "p1 2 unique visits\np2 1 unique visit\n"
    end
  end
end
