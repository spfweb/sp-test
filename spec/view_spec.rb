require "spec_helper"

require "view"

RSpec.describe "View" do
  describe "View.new" do
    it "stores and returns the page and viewer passed in" do
      v = View.new("page", "viewer")
      expect(v.page).to eq "page"
      expect(v.viewer).to eq "viewer"
    end
  end
  describe "View.parse" do
    it "returns nil view when parsing nil" do
      v = View.parse(nil)
      expect(v).to be nil
    end
    it "returns a view when parsing empty string" do
      v = View.parse("")
      expect(v.page).to be nil
      expect(v.viewer).to be nil
    end
    it "returns a view with some data when parsing a string" do
      v = View.parse("page viewer")
      expect(v.page).to eq "page"
      expect(v.viewer).to eq "viewer"
    end
    it "splits only on first whitespace" do
      v = View.parse("page viewer something")
      expect(v.page).to eq "page"
      expect(v.viewer).to eq "viewer something"
    end
    it "doesn't have a viewer if no whitespace" do
      v = View.parse("page")
      expect(v.page).to eq "page"
      expect(v.viewer).to be nil
    end
  end
end
