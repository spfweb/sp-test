require "spec_helper"

require "view"
require "view_counter"

RSpec.describe "ViewCounter" do
  describe "adding views" do
    it "ignores nil views" do
      vc = ViewCounter.new
      vc.add(nil)
      expect(vc.count).to eq 0
    end
    it "records a view" do
      vc = ViewCounter.new
      vc.add(View.new("p1", "v1"))
      expect(vc.count).to eq 1
    end
    describe "counted_views" do
      it "counts total views per page - empty counter" do
        vc = ViewCounter.new
        counted = vc.counted_views
        expect(counted.count).to eq 0
      end
      it "counts total views per page" do
        vc = ViewCounter.new
        vc.add(View.new("p1", "v1"))
        vc.add(View.new("p1", "v2"))
        counted = vc.counted_views
        expect(counted["p1"]).to eq 2
      end
    end
    describe "unique_visits" do
      it "counts unique views per page/viewer - empty counter" do
        vc = ViewCounter.new
        uniques = vc.unique_visits
        expect(uniques.count).to eq 0
      end

      it "counts unique views per page/viewer - empty counter" do
        vc = ViewCounter.new
        vc.add(View.new("p1", "v1"))
        vc.add(View.new("p1", "v1"))
        vc.add(View.new("p1", "v1"))

        vc.add(View.new("p1", "v2"))

        vc.add(View.new("p1", "v3"))

        vc.add(View.new("p2", "v1"))
        vc.add(View.new("p2", "v1"))
        vc.add(View.new("p2", "v2"))

        uniques = vc.unique_visits
        expect(uniques["p1"]).to eq 2
        expect(uniques["p2"]).to eq 1
      end
    end
  end
end
