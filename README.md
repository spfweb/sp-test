# Instructions

- Clone the repository
- `bundle install`
- `bundle exec rspec`
- `./parser.rb webserver.log`

# Output

Output from `./parser.rb webserver.log` is:

```
Visit Count
===========
/about/2 90 visits
/contact 89 visits
/index 82 visits
/about 81 visits
/help_page/1 80 visits
/home 78 visits

Unique Visits
=============
/help_page/1 3 unique visits
/contact 2 unique visits
/home 2 unique visits
/about/2 4 unique visits
/index 4 unique visits
/about 3 unique visits
```
