class View
  attr_reader :page, :viewer

  def self.parse(line)
    return nil if line.nil?
    page, viewer = line.split(/\s+/, 2)
    View.new(page, viewer)
  end

  def initialize(page, viewer)
    @page = page
    @viewer = viewer
  end
end
