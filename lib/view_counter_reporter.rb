class ViewCounterReporter
  def report_visit_count(view_counter, output)
    views = view_counter.counted_views.map do |h, v|
      [h, v]
    end
    reverse_sorted_views = views.sort_by { |v| -v[1] }
    reverse_sorted_views.each do |sv|
      output.puts("#{sv[0]} #{sv[1]} visit#{"s" if sv[1] > 1}")
    end
  end

  def report_unique_visits(view_counter, output)
    view_counter.unique_visits.each do |page, num|
      output.puts("#{page} #{num} unique visit#{"s" if num > 1}")
    end
  end
end
