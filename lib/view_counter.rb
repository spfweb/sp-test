class ViewCounter
  def initialize
    @views = []
  end

  def add(view)
    unless view.nil?
      @views << view
    end
  end

  def count
    @views.count
  end

  def counted_views
    view_hash = {}
    @views.each do |v|
      view_hash[v.page] = (view_hash[v.page] || 0) + 1
    end
    view_hash
  end

  def unique_visits
    counts = {}
    @views.each do |v|
      if counts.has_key?(v.page)
        counts[v.page][v.viewer] = (counts[v.page][v.viewer] || 0) + 1
      else
        counts[v.page] = { v.viewer => 1 }
      end
    end
    uniques = {}
    counts.each do |h, v|
      num_uniques = v.reduce(0) { |total, (viewer, count)| total + (count == 1 ? 1 : 0) }
      uniques[h] = num_uniques
    end
    uniques
  end
end
